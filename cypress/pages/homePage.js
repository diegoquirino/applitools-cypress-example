import BasePage from "./basePage";

class HomePage extends BasePage {

    visit() {
        cy.visit(Cypress.env("baseUrl"))
        this.getSnapshot("Página inicial")
    }

}

export default HomePage;