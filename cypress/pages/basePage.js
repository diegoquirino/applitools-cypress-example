class BasePage {

    getSnapshot(tag = '') {
        cy.eyesCheckWindow({
            tag: tag,
            target: 'window',
            fully: true
        });
    }
    
}

export default BasePage;