import { faker } from "@faker-js/faker";
import BasePage from "./basePage";

class ContactPage extends BasePage {

    visit() {
        cy.visit('/contact')
        this.getSnapshot("Formulário de Contato")
    }

    clickContactFromPage() {
        cy.get("div > .card-footer > a").contains("Contato").click()
        this.getSnapshot("Formulário de Contato")
    }

    clickContactFromMenu() {
        cy.get("a > .nav-link").contains("Contato").click()
        this.getSnapshot("Formulário de Contato")
    }

    fillForm(type, age, ...fields) {
        const {name, email, message} = {...fields}
        cy.get("#nome").clear().type(name? name : faker.name.findName())
        cy.get("#email").clear().type(email? email : faker.internet.email())
        cy.get("#mensagem").clear().type(message? message : faker.lorem.paragraph())
        cy.get('[type="radio"]').check(type)
        cy.get('#idade').select(age)
        this.getSnapshot("Formulário de Contato Preenchido")
    }

    sendMessage() {
        cy.get('form').submit()
        this.getSnapshot("Formulário de Contato Enviado")
    }
    
}

export default ContactPage;