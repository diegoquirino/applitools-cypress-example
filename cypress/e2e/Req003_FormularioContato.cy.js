import { faker } from '@faker-js/faker';
import HomePage from '../pages/homePage'
import ContactPage from '../pages/contactPage'

const homePage = new HomePage()
const contactPage = new ContactPage()

describe('REQ003 - Formulário de Contato', () => {

  beforeEach(() => {  
    // Abrir Eyes para iniciar os testes visuais.
    // Cada teste deverá abrir seu próprio Eyes para armazenar os próprios 'snapshots'.
    cy.eyesOpen({
      appName: Cypress.env("myAppName"), // O nome da aplicação que está sendo testada
      testName: Cypress.currentTest.title, // O nome do caso de teste
    })

  })

  for(let type of ["Dúvida", "Sugestão", "Reclamação"]) {
    for(let age of ["Menor que 18 anos", "Entre 18 e 29 anos", "Entre 30 e 64 anos", "A partir de 65 anos"]) {
      it(`Deve verificar que é possível enviar o contato quando o tipo de mensagem é '${type}' e a idade é '${age}'`, () => {
        // 1. Visite a página inicial
        homePage.visit()    
        // 2. Clique na opção 'Contato' da página
        contactPage.clickContactFromPage()
        // 3. Preencha o formulário com dados válidos
        contactPage.fillForm(type, age)
        // 4. Envie o formulário de contato
        contactPage.sendMessage()
        
        // 5. Testar visualmente o resultado final na tela
        contactPage.getSnapshot(`Enviar contato de '${type}' por '${age}'.`)
        
        cy.get('.alert-success')
          .should('have.length', 1)
          .and('contain', 'foi enviada com sucesso!')
          .and('contain', type.toUpperCase())
          .and('contain', age.toUpperCase())
    
      })
    }
  }

  afterEach(function() {    
    // Fechar Eyes para dizer ao Applitools que ele deve mostrar os resultados.
    cy.eyesClose()
  })

})