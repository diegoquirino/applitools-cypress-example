# Applitools Cypress Example

## Pré-requisitos

1) Para executar o exemplo corretamente, é necessário [instalar o NodeJS](https://nodejs.org/en/)

## Instalação

Este projeto já vem pré-configurado com uma seção de testes no [protótipo de alto nível/HTML do aplicativo calculadora de descontos](https://calculadora.diegoquirino.net). 

Para realizar a execução dos testes, é necessário:

1) Instalar as dependências do projeto: `npm i`
2) Em seguida, executar o _cypress_: `npx cypress open` OU `npx cypress run`

Se houver dúvidas quanto às APIs ou configurações específicas do Cypress e do Applitools, considere estudar a documentação dos mesmos a partir dos tutoriais disponíveis abaixo:

* [Tutorial Applitools](https://applitools.com/tutorials/)
* [Documentação do Cypress](https://docs.cypress.io/guides/getting-started/installing-cypress)

## Atividade Proposta

*Proceda com a criação de cenários de testes regressivos para o REQ001 – Calcular Desconto de Produto. Interpole, na classe de passos, captura de telas para a realização dos testes visuais:*

1) [Acesso ao requisito](https://gitlab.com/diegoquirino/testedesoftwareunipe/-/wikis/req001).
2) [Acesso ao formulário de envio do trabalho](https://forms.gle/QGQVp6RTgUPZ55B29). _Formato preferível da entrega: GIT (link)_

## Contato

- *Prof. Carlos Diego Quirino Lima*
- *Email:* [diegoquirino@gmail.com](mailto:diegoquirino@gmail.com)